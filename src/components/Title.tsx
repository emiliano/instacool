import * as React from 'react'

const style = {
    color:'#555'
}

class Card extends React.Component {
  public render() {
    return (
      <h2 {...this.props} style={style} />
    )
  }
}

export default Card